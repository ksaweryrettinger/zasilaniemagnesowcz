#include "cModbusMaster.h"

cModbusMaster::cModbusMaster(cEntry* pEntry, cMain* pMain, cMainError* pMainError)
{
    this->pMain = pMain;

    //Copy data from pEntry class
	this->mbAddress = pEntry->mbAddress;
	this->mbPort = pEntry->mbPort;
	this->ZNA_Address = pEntry->ZNA_Address;
	this->ODC_Address = pEntry->ODC_Address;
	this->ZAP_Address = pEntry->ZAP_Address;

	//Copy MK-DAC register addressing
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		this->MK_DAC[i][0] = pEntry->MK_DAC[i][0];
		this->MK_DAC[i][1] = pEntry->MK_DAC[i][1];
	}

	//Initialization
    ctxRTU = nullptr;
    bStartMain = false;
    bStartMainError = false;
    bModbusError = false;
    bZMKPowered = false;
    ZNA = 0;
    eActiveTract = C2;
    ThreadTCP = NULL;

    //Create Modbus Mapping (TCP/IP Server)
    mb_mapping = modbus_mapping_new_start_address(1, MB_NUM_COILS, 10001, MB_NUM_DISCRETE_INPUTS,
    		40001, MB_NUM_HOLDING_REGISTERS, 30001, MB_NUM_INPUT_REGISTERS);

    //Start main (Modbus RTU) thread
    ThreadReadCurrentRTU = new cThreadReadCurrentRTU(this);

    if (ThreadReadCurrentRTU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus RTU thread!");
        delete ThreadReadCurrentRTU;
        ThreadReadCurrentRTU = nullptr;
    }

	pMain->mbMaster = this;
	pMainError->mbMaster = this;
}

//Start Modbus TCP thread
void cModbusMaster::StartTCP(void)
{
	if (!ThreadTCP)
	{
		ThreadTCP = new cThreadTCP(this);

		if (ThreadTCP->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create Modbus TCP thread!");
			delete ThreadTCP;
			ThreadTCP = nullptr;
	   }
	}
}

//Set current in single PSU
void cModbusMaster::SetCurrent(uint8_t unitID, float current)
{
	//Create new thread for writing new current value to PSU
	ThreadSetCurrentRTU[unitID - 1] = new cThreadSetCurrentRTU(this, unitID, current);

    if (ThreadSetCurrentRTU[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create PSU Write thread!");
        delete ThreadSetCurrentRTU[unitID - 1];
        ThreadSetCurrentRTU[unitID - 1] = NULL;
    }
}

//Set polarity of single PSU
void cModbusMaster::SetPolarity(uint8_t unitID, bool bSetNegative)
{
	//Create new thread for setting PSU polarity
	ThreadSetPolarityRTU[unitID - 1] = new cThreadSetPolarityRTU(this, unitID, bSetNegative);

    if (ThreadSetPolarityRTU[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus PSU Set-Polarity thread!");
        delete ThreadSetPolarityRTU[unitID - 1];
        ThreadSetPolarityRTU[unitID - 1] = NULL;
    }
}

//Close all connections and terminate active Modbus threads
void cModbusMaster::CloseModbus(void)
{
	//Wait for other RTU threads before terminating main RTU thread

	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		while (ThreadSetCurrentRTU[i] != NULL) wxMilliSleep(1);
		while (ThreadSetPolarityRTU[i] != NULL) wxMilliSleep(1);
	}

	//Delete TCP/IP thread
	if (ThreadTCP)
	{
		//Delete thread
		if (ThreadTCP->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete TCP/IP thread!");
		while (ThreadTCP != NULL) wxMilliSleep(1);
	}

	//Delete RTU thread
	if (ThreadReadCurrentRTU != NULL)
	{
		if (ThreadReadCurrentRTU->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete Main RTU thread!");
	}

	//Wait for RTU thread termination
    while (ThreadReadCurrentRTU != NULL) wxMilliSleep(1);
}
