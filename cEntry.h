#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "ZMKConstants.h"
#include "Images/Icon.xpm"
#include "cMain.h"
#include "cMainError.h"
#include "cThreadSetCurrent.h"
#include "cModbusMaster.h"

class cMain;
class cMainError;
class cModbusMaster;
class cTimerHighRes;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cMainError* pMainError;
    cModbusMaster* mbMaster;
    wxSingleInstanceChecker* wxChecker;

private: //Modbus configuration
    int32_t mbAddress;
    wxString mbPort;

private: //Bit Register addresses
	uint16_t MK_DAC[NUM_POWER_UNITS][2];
	uint16_t ZNA_Address;
	uint16_t ODC_Address;
	uint16_t ZAP_Address;

private: //Configuration file variables
    wxTextFile tConfigFile;
    wxString filename;
    wxString strConfig;

private: //other variables
    int32_t dlgUpdate;
    wxString temp;
    cTimerHighRes* TimerInit;
    bool bConfigError;

    friend class cModbusMaster;
};

#pragma GCC diagnostic pop
