#include "cMain.h"

/* Set Current Button event */
void cMain::OnButtonSetCurrent(wxCommandEvent& evt)
{
	//Get button ID
	int32_t btnID = evt.GetId();
	int32_t unitID = btnID - 20000;

	if (!mbMaster->ThreadSetCurrentRTU[unitID - 1])
	{
		float fNewCurrent = 0;

		//Disable button during current change operation
		if (btnCurrentZMK[unitID - 1]->IsEnabled()) btnCurrentZMK[unitID - 1]->Disable();

		//Read current setting
		wxString strCurrent = txtCtrlCurrentZMK[unitID - 1]->GetValue();
		fNewCurrent = wxAtof(strCurrent);

		//Check current limits
		if (unitID < TK_START) //MK and LI Magnets
		{
			if (fNewCurrent < (MAX_CURRENT_MK * (-1))) fNewCurrent = MAX_CURRENT_MK * (-1);
			else if (fNewCurrent > MAX_CURRENT_MK) fNewCurrent = MAX_CURRENT_MK;
			txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));
		}
		else //TK Magnet
		{
			if (fNewCurrent < 0) fNewCurrent = 0;
			else if (fNewCurrent > MAX_CURRENT_TK) fNewCurrent = MAX_CURRENT_TK;
			txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));
		}

		//Start new set-current thread
		ThreadSetCurrent[unitID - 1] = new cThreadSetCurrent(this, mbMaster, unitID, fNewCurrent);

		if (ThreadSetCurrent[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create set-current thread!");
			delete ThreadSetCurrent[unitID - 1];
			ThreadSetCurrent[unitID - 1] = NULL;
		}
	}

    evt.Skip();
}
