#pragma once

#include "ZMKConstants.h"
#include "cModbusMaster.h"

class cModbusMaster;

class cPowerCheck : public wxFrame
{
	public: //constructor
		cPowerCheck(cModbusMaster*);

	private: //event handlers
		void OnRefreshDisplay(wxTimerEvent& evt);
		void RefreshDisplay(void);
		void TerminateApp(void);

	private: //class pointers
		cModbusMaster* mbMaster;
		wxStaticBitmap* imgWarning;
		wxStaticText* txtWarning;
		wxFont myFont;

	private: //Shared Data
		bool bModbusError;
		bool bZMKPowered;

	private: //other
		wxTimer* tRefreshTimer;

		wxDECLARE_EVENT_TABLE();
};
