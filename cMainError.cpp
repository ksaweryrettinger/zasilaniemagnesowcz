#include "cMainError.h"

wxBEGIN_EVENT_TABLE(cMainError, wxFrame)
	EVT_MENU(wxID_CLOSE, cMainError::OnMenuClickedExit)
	EVT_TIMER(20014, cMainError::OnRefreshDisplay)
    EVT_CLOSE(cMainError::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMainError::cMainError(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(330, 250), wxSize(600, 457), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych Czarnych"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    mbMaster = nullptr;
    this->pEntry = pEntry;

    //Other variables initialization
    bModbusError = false;
    bCurrentOK = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mMenuBar->SetBackgroundColour(wxColour(221, 220, 220));
	mHelpMenu = new wxMenu();
	mCloseMenu = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		mCloseMenu->Append(wxID_CLOSE, _T("Zakończ"));
		mMenuBar->Append(mCloseMenu, _T("Koniec Pracy"));
		SetMenuBar(mMenuBar);
	}

	//TITLE
	txtTitleMain = new wxStaticText(this, wxID_ANY, _T("Błąd w Inicjalizacji Zasilaczy"), wxPoint(0, 21), wxSize(600, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleMain->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleMain->SetForegroundColour(wxColour(207, 31, 37));

	//PANELS
	for (uint8_t mkNum = 0; mkNum < NUM_MK_READ; mkNum++)
	{
		if (mkNum < 5) panMK[mkNum] = new wxPanel(this, wxID_ANY, wxPoint(30, 60 + mkNum*65), wxSize(260, 55), wxSUNKEN_BORDER);
		else panMK[mkNum] = new wxPanel(this, wxID_ANY, wxPoint(310, 60 + (mkNum - 5)*65), wxSize(260, 55), wxSUNKEN_BORDER);

		//Draw panel divisor lines
		wxStaticLine* lineV1 = new wxStaticLine(panMK[mkNum], wxID_ANY, wxPoint(115, 0), wxSize(1, 54), wxLI_VERTICAL);
		wxStaticLine* lineV2 = new wxStaticLine(panMK[mkNum], wxID_ANY, wxPoint(200, 0), wxSize(1, 54), wxLI_VERTICAL);
		lineV1->SetBackgroundColour(wxColour(186, 186, 186));
		lineV2->SetBackgroundColour(wxColour(186, 186, 186));

		//Draw ZMK Title
		if (mkNum >= 8)
		{
			wxStaticText* mkTitle = new wxStaticText(panMK[mkNum], wxID_ANY, MK_NAMES_ERROR[mkNum], wxPoint(14, 9), wxSize(90, 40), wxALIGN_CENTRE_HORIZONTAL);
			mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
		}
		else
		{
			wxStaticText* mkTitle = new wxStaticText(panMK[mkNum], wxID_ANY, MK_NAMES_ERROR[mkNum], wxPoint(14, 19), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
			mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
		}


		//Draw current reading
		txtCurrentMK[mkNum] = new wxStaticText(panMK[mkNum], wxID_ANY, wxString::Format(wxT("%.2fA"),
				fCurrent[mkNum]), wxPoint(108, 18), wxSize(100, 20), wxALIGN_CENTRE_HORIZONTAL);
		txtCurrentMK[mkNum]->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

		//Draw Modbus Error Images
		imgModbusError[mkNum] = new wxStaticBitmap(panMK[mkNum], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/Modbus_Error.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 8), wxSize(40, 40));
		imgModbusError[mkNum]->Hide();

		//Draw Current Error Images
		imgCurrentError[mkNum] = new wxStaticBitmap(panMK[mkNum], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Error.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 7), wxSize(40, 40));
		imgCurrentError[mkNum]->Hide();

		//Draw Current OK Images
		imgCurrentOK[mkNum] = new wxStaticBitmap(panMK[mkNum], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Ready.png"),
				wxBITMAP_TYPE_PNG), wxPoint(209, 8), wxSize(40, 40));
		imgCurrentOK[mkNum]->Hide();
	}

	 tRefreshTimer = new wxTimer(this, 20014);
	 tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

void cMainError::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;

		for (uint8_t mkNum = 0; mkNum < NUM_MK_READ; mkNum++)
		{
			//Copy data
			bPSUTimeout[mkNum] = mbMaster->bPSUTimeout[mkNum];
			bPSUOverrun[mkNum] = mbMaster->bPSUOverrun[mkNum];
			bInitTimeout[mkNum] = mbMaster->bInitTimeout[mkNum];
			fCurrent[mkNum] = mbMaster->fCurrent[mkNum];
		}
	}

	/******************************************* Update Panels *********************************************/

	bCurrentOK = true;

	for (uint8_t mkNum = 0; mkNum < NUM_MK_READ; mkNum++)
	{
		if (bInitTimeout[mkNum] && fCurrent[mkNum] < MIN_SAFE_CURRENT) bInitTimeout[mkNum] = false; //CLEAR TIMEOUT ERROR

		if (bModbusError) //DISCONNECTED
		{
			if (imgCurrentOK[mkNum]->IsShown()) imgCurrentOK[mkNum]->Hide();
			if (imgCurrentError[mkNum]->IsShown()) imgCurrentError[mkNum]->Hide();
			if (!imgModbusError[mkNum]->IsShown()) imgModbusError[mkNum]->Show();
			txtCurrentMK[mkNum]->SetLabel(wxString("-"));

			//Current errors
			bCurrentOK = false;
		}
		else //CONNECTED
		{
			//Hide Modbus errors
			if (imgModbusError[mkNum]->IsShown()) imgModbusError[mkNum]->Hide();

			//Display errors
			if (bPSUTimeout[mkNum] || bPSUOverrun[mkNum])
			{
				if (imgModbusError[mkNum]->IsShown()) imgModbusError[mkNum]->Hide();
				if (imgCurrentOK[mkNum]->IsShown()) imgCurrentOK[mkNum]->Hide();
				if (!imgCurrentError[mkNum]->IsShown()) imgCurrentError[mkNum]->Show();

				//Cannot read current
				txtCurrentMK[mkNum]->SetLabel(wxString("-"));

				//Current errors
				bCurrentOK = false;
			}
			else if (bInitTimeout[mkNum])
			{
				if (imgModbusError[mkNum]->IsShown()) imgModbusError[mkNum]->Hide();
				if (imgCurrentOK[mkNum]->IsShown()) imgCurrentOK[mkNum]->Hide();
				if (!imgCurrentError[mkNum]->IsShown()) imgCurrentError[mkNum]->Show();

				//Update current - no information regarding polarities in error mode
				txtCurrentMK[mkNum]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[mkNum]));

				//Current errors
				bCurrentOK = false;
			}
			else //No Errors, display Current OK
			{
				if (imgModbusError[mkNum]->IsShown()) imgModbusError[mkNum]->Hide();
				if (imgCurrentError[mkNum]->IsShown()) imgCurrentError[mkNum]->Hide();
				if (!imgCurrentOK[mkNum]->IsShown()) imgCurrentOK[mkNum]->Show();

				//Update current - no information regarding polarities in error mode
				txtCurrentMK[mkNum]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[mkNum]));
			}
		}
	}

	if (bCurrentOK)
	{
		txtTitleMain->SetLabel(_T("Gotowość Zasilaczy"));
		txtTitleMain->SetForegroundColour(wxColour(34, 188, 34));
	}
	else
	{
		txtTitleMain->SetLabel(_T("Błąd w Inicjalizacji Zasilaczy"));
		txtTitleMain->SetForegroundColour(wxColour(207, 31, 37));
	}

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMainError::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

/* Frame closed */
void cMainError::OnClose(wxCloseEvent&)
{
	TerminateApp();
}

/* Frame closed from menu */
void cMainError::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp();
}

void cMainError::TerminateApp()
{
	/******************************************* Stop Refresh **********************************************/

	tRefreshTimer->Stop();

	/******************************************* Destroy Frame *********************************************/

	Destroy();
}
