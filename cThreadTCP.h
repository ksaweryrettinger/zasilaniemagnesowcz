#pragma once

#include "ZMKConstants.h"
#include "cModbusMaster.h"

class cThreadTCP;
class cModbusMaster;

DECLARE_EVENT_TYPE(wxEVT_REMOTE_SET_CURRENT, -1)

/* Modbus TCP/IP thread */
class cThreadTCP : public wxThread
{
	public:
		cThreadTCP(cModbusMaster*);
		~cThreadTCP();

	protected:
		virtual ExitCode Entry();

	private:
		void RestartSocketTCP(modbus_t*);

	private: //TCP/IP configuration
		cModbusMaster* mbMaster;
		uint8_t *tcpQuery;
		modbus_t* ctxTCP;
		int32_t socket;
		int32_t rc;
		int32_t rp;
		int32_t ct;

	private:
		uint8_t unitID;
		uint16_t regHoldingTemp;
		eTract eActiveTract;
		int32_t regNum;
		int32_t regStart;
		char cCurrent[6] = { 0 };
		bool bCurrentSet;
};
