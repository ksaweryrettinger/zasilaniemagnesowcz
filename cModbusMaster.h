#pragma once

#include "ZMKConstants.h"
#include "cEntry.h"
#include "cMain.h"
#include "cMainError.h"
#include "cModbusRTU.h"
#include "cThreadTCP.h"

class cEntry;
class cMain;
class cMainError;
class cThreadTCP;
class cThreadReadCurrentRTU;
class cThreadSetCurrentRTU;
class cThreadSetPolarityRTU;

/* Modbus Gateway class */
class cModbusMaster
{
	public:
		cModbusMaster(cEntry*, cMain*, cMainError*);
		void CloseModbus(void);
		void StartTCP(void);
		void SetCurrent(uint8_t, float);
		void SetPolarity(uint8_t, bool);

	public: //class pointers and flags
		cMain* pMain;
		bool bStartMain;
		bool bStartMainError;

	public: //modbus
		int32_t mbAddress;
		wxString mbPort;
		modbus_t* ctxRTU;
		modbus_mapping_t* mb_mapping;

	public: //critical sections
		wxCriticalSection mb_guard; //prevent simultaneous Modbus RTU operations
		wxCriticalSection local_guard; //prevent simultaneous local data access
		wxCriticalSection remote_guard; //prevent simultaneous remote data access

	public: //threads
		cThreadTCP* ThreadTCP;
		cThreadReadCurrentRTU* ThreadReadCurrentRTU;
		cThreadSetCurrentRTU* ThreadSetCurrentRTU[NUM_POWER_UNITS] = { NULL };
		cThreadSetPolarityRTU* ThreadSetPolarityRTU[NUM_POWER_UNITS] = { NULL };;

	public: //SHARED DATA

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };
		bool bInitTimeout[NUM_MK_READ] = { false };

		//Readings
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		float fCurrentSetting[NUM_POWER_UNITS] = { 0 };

		//Other
		uint16_t ZNA;
		eTract eActiveTract;
		bool bZMKPowered;

	private: //register addressing
		uint16_t MK_DAC[NUM_POWER_UNITS][2];
		uint16_t ZNA_Address;
		uint16_t ODC_Address;
		uint16_t ZAP_Address;

		friend class cThreadReadCurrentRTU;
		friend class cThreadSetCurrentRTU;
		friend class cThreadSetPolarityRTU;
};
