#pragma once

#include "ZMKConstants.h"
#include "cModbusMaster.h"

class cEntry;
class cModbusMaster;
class cConstants;

class cMainError : public wxFrame
{
	public: //constructor
		cMainError(cEntry*);
		void TerminateApp(void);

	private: //event handlers
		void OnMenuClickedExit(wxCommandEvent& evt);
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cModbusMaster* mbMaster;
		wxMenu* mHelpMenu;

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mCloseMenu;

	private: //UI elements

		//TIMERS
		wxTimer* tRefreshTimer;

		//PANELS
		wxPanel* panMK[NUM_MK_READ] = { NULL };

		//IMAGES
		wxStaticBitmap* imgModbusError[NUM_MK_READ];
		wxStaticBitmap* imgCurrentError[NUM_MK_READ];
		wxStaticBitmap* imgCurrentOK[NUM_MK_READ];

		//FONTS
		wxFont myFont;

		//STATIC TEXTS
		wxStaticText* txtTitleMain;
		wxStaticText* txtTitleMK[NUM_MK_READ] = { NULL };
		wxStaticText* txtCurrentMK[NUM_MK_READ] = { NULL };

	private: //Shared Data

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_MK_READ] = { false };
		bool bPSUOverrun[NUM_MK_READ] = { false };
		bool bInitTimeout[NUM_MK_READ] = { false };
		bool bCurrentOK;

		//Current data
		float fCurrent[NUM_POWER_UNITS] = { 0 };

		wxDECLARE_EVENT_TABLE();
};


