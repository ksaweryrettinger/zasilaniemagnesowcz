#pragma once

//EXTERNAL LIBRARIES
#include <boost/algorithm/string.hpp>
#include <regex.h>
#include <string>
#include <locale>
#include <vector>
#include <modbus.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <limits.h>
#include <map>
#include <wx/wx.h>
#include <wx/log.h>
#include <wx/textfile.h>
#include <wx/snglinst.h>
#include <wx/progdlg.h>
#include <wx/statline.h>

//NAMESPACES
using namespace std;

//DEBUG MODES
const bool CHECK_INIT_ERRORS	  = true; //default: true
const bool CHECK_CURRENT_ERRORS   = true; //default: true
const bool STORE_CURRENT_READINGS = true; //default: true (false = display current settings as readings)

//APPLICATION MODES AND TRACTS
typedef enum { Local = 0, Remote = 1} eMode;
typedef enum { D = 0, C2 = 1, None = 2} eTract;

//PSU INFO
const int32_t NUM_POWER_UNITS = 13;
const int32_t NUM_TRACTS = 2;
const int32_t NUM_MK_READ = 10;
const int32_t NUM_MK_PAN = 6;
const int32_t TK_PAN = 2;
const int32_t LI_PAN = 5;
const uint8_t TK_START = 11;

//CURRENT CONFIGURATION
const float ADC_1A_WRITE_MK = 511.875f;
const float ADC_1A_WRITE_TK = 170.625f;
const float ADC_1A_READ = 291.534f;
const float MIN_SAFE_CURRENT = 0.1f;
const float MAX_CURRENT_MK = 8.0f;
const float MAX_CURRENT_TK = 24.0f;
const float H_AMP = 0.5f;

//TRACT NAMES
const wxString TRACT_NAMES[NUM_TRACTS] = {'D', "C-2"};

//MAGNET NAMES - ERROR MODE
const wxString MK_NAMES_ERROR[NUM_MK_READ] = {"MK20 (A)",
										 	  "MK20 (B)",
											  "MK21 (A)",
											  "MK21 (B)",
											  "MK23 (A)",
											  "MK23 (B)",
											  "MK24 (A)",
											  "MK24 (B)",
											  "Linia\nIniekcyjna (A)",
											  "Linia\nIniekcyjna (B)"};

//MAGNET NAMES - NORMAL MODE
const wxString MK_NAMES[NUM_MK_PAN - 1] = {"MK20",
										   "MK21",
										   "TK1",
										   "MK23",
										   "MK24"};

//POWER UNIT NAMES
const wxString ZMK_NAMES[3] = {'A', 'B', 'C'};

/* TRACT - MK ARRAY INDEXES
 *
 * 0 = MK20
 * 1 = MK21
 * 2 = TK1
 * 3 = MK23
 * 4 = MK24
 * 5 = Linia Iniekcyjna
 *
 */

const vector<uint8_t> TRACTS[NUM_TRACTS] = {{0, 1, 2, 3, 4, 5}, //Tract D
											{0, 1, 3, 4, 5}}; 	//Tract C-2

//MK - ZMK ARRAY INDEXES
const vector<uint8_t> MK_ZMK[NUM_MK_PAN] = {{0, 1},       //MK20
											{2, 3},       //MK21
											{10, 11, 12}, //TK1
											{4, 5},       //MK23
											{6, 7},       //MK24
											{8, 9}};      //Linia Iniekcyjna

//MK POLARITY BIT MASKS
const uint16_t MK_ZNA[NUM_MK_READ] = {0x0001,   //MK20 (Góra)
								 	  0x0002,   //MK20 (Dół)
									  0x0004,   //MK21 (Góra)
									  0x0008,   //MK21 (Dół)
									  0x0040,   //MK23 (Góra)
									  0x0080,   //MK23 (Dół)
									  0x0100,   //MK24 (Góra)
									  0x0200,   //MK24 (Dół)
									  0x0010,   //Linia Iniekcyjna (Dół)
									  0x0020};  //Linia Iniekcyjna (Góra)

//MK-ZAP MAPPING (READ-CURRENT ADDRESSING)
const uint16_t MK_ZAP[NUM_MK_READ] = {0b00110,  //MK20 (Góra)
								 	  0b01110,  //MK20 (Dół)
									  0b10110,  //MK21 (Góra)
									  0b11110,  //MK21 (Dół)
									  0b10101,  //MK23 (Góra)
									  0b11101,  //MK23 (Dół)
									  0b00011,  //MK24 (Góra)
									  0b01011,  //MK24 (Dół)
									  0b00101,  //Linia Iniekcyjna (Dół)
									  0b01101}; //Linia Iniekcyjna (Góra)

//MODBUS CONFIGURATION
const int32_t MB_BAUD = 115200; //Modbus RTU Baud Rate
const int32_t MB_RESPONSE_TIMEOUT_SEC = 0; //Modbus RTU response timeout (seconds)
const int32_t MB_RESPONSE_TIMEOUT_US = 75000; //Modbus RTU response timeout (us)
const int32_t MB_INDICATION_TIMEOUT_US = 150000; //Modbus TCP/IP Indication timeout (us)

//MODBUS TCP/IP REGISTERS
const int32_t MB_NUM_COILS			   = 0;
const int32_t MB_NUM_DISCRETE_INPUTS   = 0;
const int32_t MB_NUM_INPUT_REGISTERS   = 30;
const int32_t MB_NUM_HOLDING_REGISTERS = 13;

//MODBUS TCP/IP INPUT REGISTER MAPPING
const uint8_t MB_INPUTREG_CUR_START    = 1; 	//Current Readings (START)
const uint8_t MB_INPUTREG_CUR_END      = 10;	//Current Readings (END)
const uint8_t MB_INPUTREG_SETCUR_START = 11;	//Current Settings (START)
const uint8_t MB_INPUTREG_SETCUR_END   = 23;	//Current Settings (END)
const uint8_t MB_INPUTREG_ZMK_STATUS   = 24;	//MK Power Status
const uint8_t MB_INPUTREG_ACTIVE_TRACT = 25;	//Active Tract
const uint8_t MB_INPUTREG_POLARITY     = 26;	//MK Polarities
const uint8_t MB_INPUTREG_RTU_ERROR    = 27;	//Modbus RTU Errors
const uint8_t MB_INPUTREG_PSU_TIMEOUT  = 28;	//MK PSU Timeout Errors	(10 LSB)
const uint8_t MB_INPUTREG_PSU_OVERRUN  = 29;	//MK PSU Overrun Errors (10 LSB)
const uint8_t MB_INPUTREG_USERMODE     = 30;	//User Mode (Local/Remote)

//MODBUS TCP/IP HOLDING REGISTER MAPPING
const uint8_t MB_HOLDING_SETCUR_START  = 1; 	//Remote Current Setting (START)
const uint8_t MB_HOLDING_SETCUR_END    = 13;	//Remote Current Setting (END)
const int32_t MAX_REMOTE_EVENTS = 100;

//THREAD DELAYS
const int32_t DELAY_REFRESH_DISPLAY   = 211;  //ms, main display refresh
const int32_t DELAY_CURRENT_STEP	  = 149;  //ms, delay between current steps
const int32_t DELAY_SET_CURRENT       = 997;  //ms, cThreadSetCurrent data refresh
const int32_t DELAY_CURRENT_READY 	  = 60;   //ms, minimum time before current reading is ready
const int32_t DELAY_CURRENT_REPEAT 	  = 5;    //ms, time between current read attempts
const int32_t DELAY_CURRENT_NEXTUNIT  = 50;   //ms, time between current read attempts on different PSUs
const int32_t DELAY_CONNECTION_TCP	  = 100;  //ms, delay between subsequent TCP connection attempts
const int32_t DELAY_CLOSE_APP		  = 1000; //ms, delay before closing app (successful close)

//TIMEOUTS
const int32_t INIT_PSU_TIMEOUT 		  = 6000;  //ms
const int32_t SET_CURRENT_TIMEOUT_MK  = 6000;  //ms
const int32_t SET_CURRENT_TIMEOUT_TK  = 22500; //ms
const int32_t READ_CURRENT_TIMEOUT 	  = 200;   //ms
const int32_t CLOSE_APP_TIMEOUT		  = 22500; //ms
