#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_NETWORK, cMain::OnMenuClickedMode)
	EVT_MENU(wxID_CLOSE, cMain::OnMenuClickedExit)
	EVT_COMMAND(wxID_ANY, wxEVT_REMOTE_SET_CURRENT, cMain::OnRemoteSetCurrent)
	EVT_TIMER(20014, cMain::OnRefreshDisplay)
    EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(175, 9), wxSize(850, 1005), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX & ~wxCLOSE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych Czarnych (Sterowanie Zdalne)"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    mbMaster = nullptr;
    pInformation = nullptr;
    pPowerCheck = nullptr;
    this->pEntry = pEntry;

    //Other variables initialization
    informationWindowIsOpen = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mMenuBar->SetBackgroundColour(wxColour(221, 220, 220));
	mCloseMenu = new wxMenu();
	mHelpMenu = new wxMenu();
	mModeMenu = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		//Populate drop-down menus
		mHelpMenu->Append(wxID_INFO, _T("Informacje"));
		mModeMenu->Append(wxID_NETWORK, _T("Przejdź na Sterowanie Lokalne"));
		mCloseMenu->Append(wxID_CLOSE, _T("Zakończ"));

		//Append drop-down menus to menu-bar
		mMenuBar->Append(mModeMenu, _T("Sterowanie: Zdalne"));
		mMenuBar->Append(mCloseMenu, _T("Koniec Pracy"));
		mMenuBar->Append(mHelpMenu, _T("O Programie"));
		SetMenuBar(mMenuBar);
	}

	//OTHER VARIABLES
	TimerCloseApp = new cTimerHighRes();
	mkNum = 0;
	zmkNum = 0;
	zmkIndex = 0;
	bModbusError = false;
	bZMKPowered = false;
	eUserMode = Remote;
	eNewTract = None;
	eActiveTract = None;
	bCurrentOK = false;
	ZNA = 0;

	//REMOTE CONTROL
	newCurrentNum = 0;

	//MAIN PANELS
	panTractDisplay = new wxPanel(this, wxID_ANY, wxPoint(20, 21), wxSize(251, 911), wxSUNKEN_BORDER);
	panControls = new wxPanel(this, wxID_ANY, wxPoint(290, 21), wxSize(540, 911), wxSUNKEN_BORDER);
	panZMKPower = new wxPanel(this, wxID_ANY, wxPoint(63, 823), wxSize(168, 75), wxSUNKEN_BORDER);

	//PANEL TITLES
	txtTitleLI = NULL;
	txtTitleControls = new wxStaticText(panControls, wxID_ANY, _T(" "), wxPoint(170, 18), wxSize(200, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleControls->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleZMKConnected = new wxStaticText(panZMKPower, wxID_ANY, _T("Zasilanie\nWłączone"), wxPoint(6, 21), wxSize(100, 50), wxALIGN_CENTRE_HORIZONTAL);
	txtTitleZMKConnected->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTitleZMKConnected->SetForegroundColour(wxColour(34, 188, 34));

	//ZMK CONNECTED MODBUS ERROR MESSAGE
	txtZMKPoweredMBError = new wxStaticText(panZMKPower, wxID_ANY, _T("[Błąd Modbus]"), wxPoint(4, 28), wxSize(160, 30), wxALIGN_CENTRE_HORIZONTAL);
	txtZMKPoweredMBError->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtZMKPoweredMBError->SetForegroundColour(wxColour(207, 31, 37));
	txtZMKPoweredMBError->Hide();

	//IMAGES AND BUTTONS
	for (uint8_t i = 0; i < NUM_TRACTS; i++)
	{
		//Tract Selection Images
		imgTracts[i] = new wxStaticBitmap(panTractDisplay, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/Trakt_") + wxString::Format(_T("%d.png"), i),
				wxBITMAP_TYPE_PNG), wxPoint(-118, -48), wxSize(501, 860));
		imgTracts[i]->Hide();

		//Modbus Error Tract Image (Default)
		imgTractsMBError[i] = new wxStaticBitmap(panTractDisplay, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
				wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/Trakt_") + wxString::Format(_T("%d_MB_Error.png"), i),
				wxBITMAP_TYPE_PNG), wxPoint(-118, -48), wxSize(501, 860));
		imgTractsMBError[i]->Hide();
	}

	//IMAGES
	imgZMKPowered = new wxStaticBitmap(panZMKPower, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Connected.png"), wxBITMAP_TYPE_PNG), wxPoint(98, 9), wxSize(55, 55));
	imgZMKPowered->Hide();

	imgZMKNoPower = new wxStaticBitmap(panZMKPower, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Disconnected.png"), wxBITMAP_TYPE_PNG), wxPoint(100, 9), wxSize(55, 55));
	imgZMKNoPower->Hide();

	//Bind set-current buttons to single event handler
    Bind(wxEVT_BUTTON, &cMain::OnButtonSetCurrent, this, 20001, 20013);

    tRefreshTimer = new wxTimer(this, 20014);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

void cMain::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;
		bZMKPowered = mbMaster->bZMKPowered;
		eNewTract = mbMaster->eActiveTract;
		ZNA = mbMaster->ZNA;

		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			if (i < NUM_MK_READ)
			{
				bPSUTimeout[i] = mbMaster->bPSUTimeout[i];
				bPSUOverrun[i] = mbMaster->bPSUOverrun[i];
				if ((ZNA & MK_ZNA[i]) != 0) fCurrent[i] =  mbMaster->fCurrent[i] * (-1);
				else fCurrent[i] =  mbMaster->fCurrent[i];
			}

			fCurrentSetting[i] = mbMaster->fCurrentSetting[i];
		}
	}

	/******************************************* New Tract Selection ***************************************/

	if (eActiveTract != eNewTract)
	{
		/******************************************* Delete MK-ZMK Panels **************************************/

		for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
		{
			if (panMK[i])
			{
				panMK[i]->Destroy();
				panMK[i] = NULL;
			}
		}

		/******************************************* Update Active Tract ***************************************/

		eActiveTract = eNewTract;

		/******************************************* Update Tract Display ***************************************/

		for (uint8_t i = 0; i < NUM_TRACTS; i++)
		{
			//IMAGES
			if (i == eActiveTract && !imgTracts[i]->IsShown()) imgTracts[i]->Show();
			else if (imgTracts[i]->IsShown()) imgTracts[i]->Hide();
		}

		/******************************************* Update Tract Title ****************************************/

		txtTitleControls->SetLabel(wxString(_T("TRAKT ")) + TRACT_NAMES[eActiveTract]);

		/******************************************* Draw MK-ZMK Panels ****************************************/

		zmkIndex = 1;

		for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
		{
			mkNum = TRACTS[eActiveTract][i];

			if (!panMK[i])
			{
				if (eActiveTract == D && mkNum > TK_PAN && mkNum != LI_PAN) panPosition = wxPoint(10, 53 + i*125) + wxPoint(0, 55);
				else if (eActiveTract == D && mkNum == LI_PAN) panPosition = wxPoint(10, 53 + i*125) + wxPoint(0, 105);
				else if (eActiveTract == C2 && mkNum == LI_PAN) panPosition = wxPoint(10, 53 + i*125) + wxPoint(0, 50);
				else panPosition = wxPoint(10, 53 + i*125);

				if (mkNum != TK_PAN && mkNum != LI_PAN) //MK Panel
				{
					panMK[i] = new wxPanel(panControls, wxID_ANY, panPosition, wxSize(520, 110), wxSUNKEN_BORDER);

					//Draw panel divisor lines
					wxStaticLine* lineV1 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(90, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV2 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(195, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV3 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(290, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV4 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(460, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineH = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(91, 54), wxSize(428, 1), wxLI_HORIZONTAL);
					lineV1->SetBackgroundColour(wxColour(163, 168, 173));
					lineV2->SetBackgroundColour(wxColour(163, 168, 173));
					lineV3->SetBackgroundColour(wxColour(163, 168, 173));
					lineV4->SetBackgroundColour(wxColour(163, 168, 173));
					lineH->SetBackgroundColour(wxColour(163, 168, 173));

					//Draw magnet itle
					wxStaticText* mkTitle = new wxStaticText(panMK[i], wxID_ANY, MK_NAMES[mkNum], wxPoint(1, 47), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
					mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				}
				else if (mkNum == TK_PAN && eActiveTract == D) //TK Panel
				{
					panMK[i] = new wxPanel(panControls, wxID_ANY, panPosition, wxSize(520, 165), wxSUNKEN_BORDER);

					//Draw panel divisor lines
					wxStaticLine* lineV1 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(90, 0), wxSize(1, 164), wxLI_VERTICAL);
					wxStaticLine* lineV2 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(195, 0), wxSize(1, 164), wxLI_VERTICAL);
					wxStaticLine* lineV3 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(290, 0), wxSize(1, 164), wxLI_VERTICAL);
					wxStaticLine* lineV4 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(460, 0), wxSize(1, 164), wxLI_VERTICAL);
					wxStaticLine* lineH1 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(91, 54), wxSize(428, 1), wxLI_HORIZONTAL);
					wxStaticLine* lineH2 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(91, 109), wxSize(428, 1), wxLI_HORIZONTAL);
					lineV1->SetBackgroundColour(wxColour(163, 168, 173));
					lineV2->SetBackgroundColour(wxColour(163, 168, 173));
					lineV3->SetBackgroundColour(wxColour(163, 168, 173));
					lineV4->SetBackgroundColour(wxColour(163, 168, 173));
					lineH1->SetBackgroundColour(wxColour(163, 168, 173));
					lineH2->SetBackgroundColour(wxColour(163, 168, 173));

					//Draw magnet title
					wxStaticText* mkTitle = new wxStaticText(panMK[i], wxID_ANY, MK_NAMES[mkNum], wxPoint(1, 73), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
					mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				}
				else if (mkNum == LI_PAN) //LI Panel
				{
					//Re-draw panel title
					if (txtTitleLI) txtTitleLI->Destroy();
					txtTitleLI = new wxStaticText(panControls, wxID_ANY, _T("LINIA INIEKCYJNA"), panPosition + wxPoint(0, -40), wxSize(520, 20), wxALIGN_CENTRE_HORIZONTAL);
					txtTitleLI->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

					panMK[i] = new wxPanel(panControls, wxID_ANY, panPosition, wxSize(520, 110), wxSUNKEN_BORDER);

					//Draw panel divisor lines
					wxStaticLine* lineV1 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(90, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV2 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(195, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV3 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(290, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineV4 = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(460, 0), wxSize(1, 109), wxLI_VERTICAL);
					wxStaticLine* lineH = new wxStaticLine(panMK[i], wxID_ANY, wxPoint(91, 54), wxSize(428, 1), wxLI_HORIZONTAL);
					lineV1->SetBackgroundColour(wxColour(163, 168, 173));
					lineV2->SetBackgroundColour(wxColour(163, 168, 173));
					lineV3->SetBackgroundColour(wxColour(163, 168, 173));
					lineV4->SetBackgroundColour(wxColour(163, 168, 173));
					lineH->SetBackgroundColour(wxColour(163, 168, 173));

					//Draw magnet title
					wxStaticText* mkTitle = new wxStaticText(panMK[i], wxID_ANY, "-", wxPoint(1, 47), wxSize(90, 20), wxALIGN_CENTRE_HORIZONTAL);
					mkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				}

				if (panMK[i])
				{
					//Draw ZMK information and controls
					for (uint8_t j = 0; j < MK_ZMK[mkNum].size(); j++)
					{
						zmkNum = MK_ZMK[mkNum][j];

						//Draw ZMK title
						wxStaticText* zmkTitle = new wxStaticText(panMK[i], wxID_ANY, (_T("Zasilacz ") + ZMK_NAMES[j]), wxPoint(88, 19 + j*54), wxSize(110, 20), wxALIGN_CENTRE_HORIZONTAL);
						zmkTitle->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

						//Draw current reading
						if (mkNum != TK_PAN)
						{
							txtCurrentZMK[zmkNum] = new wxStaticText(panMK[i], wxID_ANY, wxString::Format(wxT("%.2fA"),
														fCurrent[zmkNum]), wxPoint(195, 18 + j*54), wxSize(100, 20), wxALIGN_CENTRE_HORIZONTAL);
						}
						else //no current readings for TK
						{
							txtCurrentZMK[zmkNum] = new wxStaticText(panMK[i], wxID_ANY, " - ", wxPoint(195, 18 + j*54), wxSize(100, 20), wxALIGN_CENTRE_HORIZONTAL);
						}

						//Set current font
						txtCurrentZMK[zmkNum]->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

						//Draw current controls
						txtCtrlCurrentZMK[zmkNum] = new wxTextCtrl(panMK[i], wxID_ANY, wxString::Format(wxT("%.2f"),
								fCurrentSetting[zmkNum]), wxPoint(307, 13 + j*54), wxSize(70, 28), wxALIGN_CENTRE_HORIZONTAL);

						//Check user mode
						if (eUserMode == Remote)
						{
							if (txtCtrlCurrentZMK[zmkNum]->IsEnabled()) txtCtrlCurrentZMK[zmkNum]->Disable();
						}

						//Draw current units
						wxStaticText* txtCtrlCurrentUnits = new wxStaticText(panMK[i], wxID_ANY, "A", wxPoint(383, 19 + j*54), wxSize(40, 20), wxALIGN_LEFT);
						txtCtrlCurrentUnits->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

						//Draw buttons
						btnCurrentZMK[zmkNum] = new wxButton(panMK[i], 20001 + zmkNum, "OK", wxPoint(404, 13 + j*54), wxSize(40, 28), wxBU_EXACTFIT);
						btnCurrentZMK[zmkNum]->SetBackgroundColour(wxColour(215, 215, 215));

						//Check user mode
						if (eUserMode == Remote)
						{
							if (btnCurrentZMK[zmkNum]->IsEnabled()) btnCurrentZMK[zmkNum]->Disable();
						}

						//Draw Modbus Error Images
						imgModbusError[zmkNum] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
								wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/Modbus_Error.png"),
								wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
						imgModbusError[zmkNum]->Hide();

						//Draw Current Error Images
						imgCurrentError[zmkNum] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
								wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Error.png"),
								wxBITMAP_TYPE_PNG), wxPoint(469, 7 + j*54), wxSize(40, 40));
						imgCurrentError[zmkNum]->Hide();

						//Draw Current OK Images
						imgCurrentOK[zmkNum] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
								wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Ready.png"),
								wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
						imgCurrentOK[zmkNum]->Hide();

						//Draw Set Current Images
						imgCurrentSet[zmkNum] = new wxStaticBitmap(panMK[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
								wxString("/eclipse-workspace/ZasilanieMagnesowCz/Images/ZMK_Busy.png"),
								wxBITMAP_TYPE_PNG), wxPoint(469, 8 + j*54), wxSize(40, 40));
						imgCurrentSet[zmkNum]->Hide();
					}
				}
			}
		}
	}

	/******************************************* Update Main Panels *******************************************/

	if (bModbusError) //DISCONNECTED
	{
		//Display Modbus errors
		if (!txtZMKPoweredMBError->IsShown()) txtZMKPoweredMBError->Show();

		//Hide ZMK power info
		if (txtTitleZMKConnected->IsShown()) txtTitleZMKConnected->Hide();
		if (imgZMKPowered->IsShown()) imgZMKPowered->Hide();
		if (imgZMKNoPower->IsShown()) imgZMKNoPower->Hide();

		//Update tract image and buttons
		for (uint8_t i = 0; i < NUM_TRACTS; i++) if (imgTracts[i]->IsShown()) imgTracts[i]->Hide();

		//Display empty tract diagram
		if (eActiveTract == D)
		{
			if (!imgTractsMBError[0]->IsShown()) imgTractsMBError[0]->Show();
			if (imgTractsMBError[1]->IsShown()) imgTractsMBError[1]->Hide();
		}
		else if (eActiveTract == C2)
		{
			if (imgTractsMBError[0]->IsShown()) imgTractsMBError[0]->Hide();
			if (!imgTractsMBError[1]->IsShown()) imgTractsMBError[1]->Show();
		}
	}
	else //CONNECTED
	{
		//Hide Modbus errors
		if (txtZMKPoweredMBError->IsShown()) txtZMKPoweredMBError->Hide();
		if (!txtTitleZMKConnected->IsShown()) txtTitleZMKConnected->Show();

		//Display ZMK info
		if (bZMKPowered)
		{
			txtTitleZMKConnected->SetPosition(wxPoint(6, 21));
			txtTitleZMKConnected->SetLabel(_T("Zasilanie\nWłączone"));
			txtTitleZMKConnected->SetForegroundColour(wxColour(34, 188, 34));
			if (!imgZMKPowered->IsShown()) imgZMKPowered->Show();
			if (imgZMKNoPower->IsShown()) imgZMKNoPower->Hide();
		}
		else
		{
			txtTitleZMKConnected->SetPosition(wxPoint(7, 20));
			txtTitleZMKConnected->SetLabel(_T("Zasilanie\nWyłączone"));
			txtTitleZMKConnected->SetForegroundColour(wxColour(207, 31, 37));
			if (!imgZMKNoPower->IsShown()) imgZMKNoPower->Show();
			if (imgZMKPowered->IsShown()) imgZMKPowered->Hide();
		}

		//Hide empty tract diagrams
		if (imgTractsMBError[0]->IsShown()) imgTractsMBError[0]->Hide();
		if (imgTractsMBError[1]->IsShown()) imgTractsMBError[1]->Hide();

		//Display tract diagram
		if (!imgTracts[eActiveTract]->IsShown()) imgTracts[eActiveTract]->Show();
	}

	/****************************************** Update MK-ZMK Panels ******************************************/

	for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
	{
		mkNum = TRACTS[eActiveTract][i];

		//Draw ZMK information and controls
		for (uint8_t j = 0; j < MK_ZMK[mkNum].size(); j++)
		{
			zmkNum = MK_ZMK[mkNum][j];

			if (bModbusError) //DISCONNECTED
			{
				if (imgCurrentOK[zmkNum]->IsShown()) imgCurrentOK[zmkNum]->Hide();
				if (imgCurrentError[zmkNum]->IsShown()) imgCurrentError[zmkNum]->Hide();
				if (!imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Show();

				//Disable buttons
				if (btnCurrentZMK[zmkNum]->IsEnabled()) btnCurrentZMK[zmkNum]->Disable();

				//Disable current input
				if (txtCtrlCurrentZMK[zmkNum]->IsEnabled() || eUserMode == Remote)
				{
					txtCurrentZMK[zmkNum]->SetLabel(wxString("-"));
					txtCtrlCurrentZMK[zmkNum]->SetValue(wxString("-"));
					txtCtrlCurrentZMK[zmkNum]->Disable();
				}
			}
			else //CONNECTED
			{
				//Check for current-setting errors
				if (!ThreadSetCurrent[zmkNum] && (zmkNum < TK_START - 1) &&
						fabs(fCurrent[zmkNum] - fCurrentSetting[zmkNum]) >= MIN_SAFE_CURRENT)
				{
					bSetCurrentError[zmkNum] = true; //current setting error
				}
				else bSetCurrentError[zmkNum] = false; //clear  error

				//Hide Modbus errors
				if (imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Hide();

				if (eUserMode == Local)
				{
					//Enable/disable buttons
					if (!mbMaster->ThreadSetCurrentRTU[zmkNum] && !ThreadSetCurrent[zmkNum]) //set-current threads not already working
					{
						if (!btnCurrentZMK[zmkNum]->IsEnabled()) btnCurrentZMK[zmkNum]->Enable();
					}
					else if (btnCurrentZMK[zmkNum]->IsEnabled()) btnCurrentZMK[zmkNum]->Disable();

					//Enable current input
					if (!txtCtrlCurrentZMK[zmkNum]->IsEnabled())
					{
						txtCtrlCurrentZMK[zmkNum]->Enable();
						txtCtrlCurrentZMK[zmkNum]->SetValue(wxString::Format(wxT("%.2f"), fCurrentSetting[zmkNum]));
					}
				}
				else if (eUserMode == Remote)
				{
					if (btnCurrentZMK[zmkNum]->IsEnabled()) btnCurrentZMK[zmkNum]->Disable();
					if (txtCtrlCurrentZMK[zmkNum]->IsEnabled()) txtCtrlCurrentZMK[zmkNum]->Disable();
				}

				//Update status icons and text
				if (bPSUTimeout[zmkNum] || bPSUOverrun[zmkNum])
				{
					if (imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Hide();
					if (imgCurrentOK[zmkNum]->IsShown()) imgCurrentOK[zmkNum]->Hide();
					if (imgCurrentSet[zmkNum]->IsShown()) imgCurrentSet[zmkNum]->Hide();
					if (!imgCurrentError[zmkNum]->IsShown()) imgCurrentError[zmkNum]->Show();

					//Cannot read current
					txtCurrentZMK[zmkNum]->SetLabel(wxString("-"));
				}
				else if (bSetCurrentError[zmkNum])
				{
					if (imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Hide();
					if (imgCurrentOK[zmkNum]->IsShown()) imgCurrentOK[zmkNum]->Hide();
					if (imgCurrentSet[zmkNum]->IsShown()) imgCurrentSet[zmkNum]->Hide();
					if (!imgCurrentError[zmkNum]->IsShown()) imgCurrentError[zmkNum]->Show();

					//Update current reading
					if (mkNum != TK_PAN)
					{
						txtCurrentZMK[zmkNum]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum]));
					}
					else txtCurrentZMK[zmkNum]->SetLabel(wxString(" - ")); //no current readings for TK
				}
				else if (mbMaster->ThreadSetCurrentRTU[zmkNum] || ThreadSetCurrent[zmkNum]) //New current set
				{
					if (imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Hide();
					if (imgCurrentOK[zmkNum]->IsShown()) imgCurrentOK[zmkNum]->Hide();
					if (imgCurrentError[zmkNum]->IsShown()) imgCurrentError[zmkNum]->Hide();
					if (!imgCurrentSet[zmkNum]->IsShown()) imgCurrentSet[zmkNum]->Show();

					//Update current reading
					if (mkNum != TK_PAN)
					{
						txtCurrentZMK[zmkNum]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum]));
					}
					else txtCurrentZMK[zmkNum]->SetLabel(wxString(" - ")); //no current readings for TK
				}
				else //No Errors, display Current OK
				{
					if (imgModbusError[zmkNum]->IsShown()) imgModbusError[zmkNum]->Hide();
					if (imgCurrentError[zmkNum]->IsShown()) imgCurrentError[zmkNum]->Hide();
					if (imgCurrentSet[zmkNum]->IsShown()) imgCurrentSet[zmkNum]->Hide();
					if (!imgCurrentOK[zmkNum]->IsShown()) imgCurrentOK[zmkNum]->Show();

					//Update current reading
					if (mkNum != TK_PAN)
					{
						txtCurrentZMK[zmkNum]->SetLabel(wxString::Format(wxT("%.2fA"), fCurrent[zmkNum]));
					}
					else txtCurrentZMK[zmkNum]->SetLabel(wxString(" - ")); //no current readings for TK
				}
			}
		}
	}

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMain::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

/* Menu clicked event */
void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
    if (informationWindowIsOpen) //window already open
    {
        pInformation->Raise(); //show window
    }
    else
    {
        informationWindowIsOpen = true;
        pInformation = new cInformation(this);
        pInformation->SetPosition(this->GetPosition());
        pInformation->SetIcon(this->GetIcon());
        pInformation->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedMode(wxCommandEvent& evt)
{
	/************************************ Update Frame Title ***********************************/

	if (eUserMode == Local)
	{
	    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych Czarnych (Sterowanie Zdalne)"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Zdalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Lokalne"));
		eUserMode = Remote;
	}
	else if (eUserMode == Remote)
	{
	    this->SetTitle(_T("Zasilanie Magnesów Korekcyjnych Czarnych"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Lokalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Zdalne"));
		eUserMode = Local;
	}

	/************************************ Remote Data ******************************************/

	if (mbMaster != nullptr)
	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_USERMODE - 1] = (uint16_t) (eUserMode == Local);
	}

    evt.Skip();
}

/* Frame closed */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp(false);
}

/* Frame closed from menu */
void cMain::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp(false);
}

void cMain::TerminateApp(bool bInitError)
{
	/******************************************* Stop Refresh **********************************************/

	tRefreshTimer->Stop();

	if (!bInitError)
	{
		/******************************************* Copy Modbus Data ******************************************/

		if (mbMaster != nullptr)
		{
			wxCriticalSectionLocker local_lock(mbMaster->local_guard);
			bModbusError = mbMaster->bModbusError;
			bZMKPowered = mbMaster->bZMKPowered;
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) fCurrentSetting[i] = mbMaster->fCurrentSetting[i];
		}

		if (!bModbusError)
		{
			/******************************************* Zero Current **********************************************/

			//Zero current setting displays
			for (uint8_t i = 0; i < TRACTS[eActiveTract].size(); i++)
			{
				mkNum = TRACTS[eActiveTract][i];

				for (uint8_t j = 0; j < MK_ZMK[mkNum].size(); j++)
				{
					zmkNum = MK_ZMK[mkNum][j];
					txtCtrlCurrentZMK[zmkNum]->SetValue(_T("0.00"));
				}
			}

			//Terminate any active set-current threads
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				if (ThreadSetCurrent[i] != NULL)
				{
					ThreadSetCurrent[i]->Delete();
					while (ThreadSetCurrent[i] != NULL) wxMilliSleep(1);
				}

				if (mbMaster->ThreadSetCurrentRTU[i] != NULL)
				{
					mbMaster->ThreadSetCurrentRTU[i]->Delete();
					while (mbMaster->ThreadSetCurrentRTU[i] != NULL) wxMilliSleep(1);
				}
			}

			//Zero all currents
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				//Start new set-current thread
				ThreadSetCurrent[i] = new cThreadSetCurrent(this, mbMaster, i + 1, 0);

				if (ThreadSetCurrent[i]->Run() != wxTHREAD_NO_ERROR)
				{
					wxLogError("Can't create set-current thread!");
					delete ThreadSetCurrent[i];
					ThreadSetCurrent[i] = NULL;
				}
			}

			//Reset timer
			TimerCloseApp->reset();

			//Wait until currents reach safety limits
			while (!bCurrentOK && !bModbusError && TimerCloseApp->elapsed() <= CLOSE_APP_TIMEOUT)
			{
				//Reset flag
				bCurrentOK = true;

				//Check current readings
				for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
				{
					if (fabs(fCurrent[i]) >= MIN_SAFE_CURRENT || bPSUTimeout[i] || bPSUOverrun[i])
					{
						bCurrentOK = false;
						break;
					}

					if ((i >= TK_START - 1) && fCurrentSetting[i] >= MIN_SAFE_CURRENT)
					{
						bCurrentOK = false;
						break;
					}
				}

				wxMilliSleep(DELAY_REFRESH_DISPLAY);
				RefreshDisplay(); //updates Modbus data as well
			}

			if (!bCurrentOK)
			{
				//Display user dialog
				if (wxMessageBox(_T("\nBłąd w zerowaniu prądu w zasilaczach.\nKoniec Pracy?"),
						_T("Koniec Pracy"), wxOK | wxCANCEL | wxICON_ERROR) != wxOK)
				{
					tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
					return;
				}
			}

			//Wait for all zero-current threads to finish
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				while (ThreadSetCurrent[i] != NULL) wxMilliSleep(1);
				while (mbMaster->ThreadSetCurrentRTU[i] != NULL) wxMilliSleep(1);
			}

			RefreshDisplay(); //updates Modbus data as well

			if (bZMKPowered)
			{
				this->Hide();
		        pPowerCheck = new cPowerCheck(mbMaster);
		        pPowerCheck->SetIcon(wxICON(icon));
		        pPowerCheck->Show();
		        pPowerCheck->SetFocus();
			}
			else wxMilliSleep(DELAY_CLOSE_APP); //close app after delay
		}
	}
	else
	{
		//Wait for all set-current threads to finish
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			while (ThreadSetCurrent[i] != NULL) wxMilliSleep(1);
			while (mbMaster->ThreadSetCurrentRTU[i] != NULL) wxMilliSleep(1);
		}
	}

	/******************************************* Close Windows *********************************************/

	if (informationWindowIsOpen) pInformation->Close();

	/******************************************* Clear Modbus Pointer **************************************/

	if (mbMaster != nullptr) mbMaster->pMain = NULL;

	/******************************************* Destroy Frame *********************************************/

	Destroy();
}
