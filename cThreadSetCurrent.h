#pragma once

#include "ZMKConstants.h"
#include "cTimerHighRes.h"
#include "cMain.h"

class cMain;
class cModbusMaster;
class cTimerHighRes;

/* System clock thread */
class cThreadSetCurrent : public wxThread
{
	public:
		cThreadSetCurrent(cMain*, cModbusMaster*, uint8_t, float);
		~cThreadSetCurrent();

	protected:
		virtual ExitCode Entry();

	private:
		void RefreshModbusData(void);
		bool TestErrors(void);

	private:
		cMain* pMain;
		cModbusMaster* mbMaster;

	private:
		float fCurrent;
		float fCurrentSetting;
		float fNewCurrent;
		bool bPSUTimeout;
		bool bPSUOverrun;
		bool bModbusError;
		bool bTimeout;
		bool bCurrentNegative;
		uint8_t unitID;
		uint16_t ZNA;
		cTimerHighRes* TimerSetCurrent;
};
