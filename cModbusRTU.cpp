#include "cModbusRTU.h"

/**************************************** Read Current Thread **********************************************/

cThreadReadCurrentRTU::cThreadReadCurrentRTU(cModbusMaster* mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	//Variables shared with Modbus Master
    eActiveTract = C2;
    IOExpander = 0;
    ZNA = 0;

	//Other variables
	TimerInitPSU = new cTimerHighRes();
	TimerReadCurrent = new cTimerHighRes();
	this->mbMaster = mbMaster;
    zmkZap = 0;
    rdBuffer = 0;
	rdStatus = false;
    rc = -1;
    bInitSuccess = false;

	//Create new RTU configuration
    mbMaster->ctxRTU = modbus_new_rtu(mbMaster->mbPort.mb_str(), MB_BAUD, 'E', 8, 1);
	modbus_set_slave(mbMaster->ctxRTU, mbMaster->mbAddress);
	modbus_rtu_set_serial_mode(mbMaster->ctxRTU, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbMaster->ctxRTU, MB_RESPONSE_TIMEOUT_SEC, MB_RESPONSE_TIMEOUT_US);
	ct = modbus_connect(mbMaster->ctxRTU);
	bModbusError = (bool) (ct == -1);

	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		mbMaster->bModbusError = bModbusError;
	}

	/************************************ Start Main (Error Mode) ******************************/

	//Modbus connection error
	if (bModbusError)
    {
    	//Signal program to start main window
    	if (CHECK_INIT_ERRORS && !mbMaster->bStartMainError) mbMaster->bStartMainError = true;
    	else if (!mbMaster->bStartMain) mbMaster->bStartMain = true;

    	/************************************ Remote Data ******************************************/

    	{
    		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
    		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;
    	}
    }
}

wxThread::ExitCode cThreadReadCurrentRTU::Entry()
{
	if (!bModbusError)
	{
		/************************************ PSU Initialization ************************************/

		bInitSuccess = InitialisePSU();
		if (bInitSuccess) mbMaster->StartTCP(); //start TCP/IP thread

		/************************************ Main RTU Loop *****************************************/

		while (!TestDestroy())
		{
			bModbusError = false;

			/************************************ Read I/O Expander ************************************/

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_read_registers(mbMaster->ctxRTU, 2048, 1, &IOExpander);
				if (rc == -1) bModbusError = true;
			}

			/************************************ Get Active Tract *************************************/

			eActiveTract = (eTract) ((IOExpander >> 1) & 1);

			/************************************* Read Currents ***************************************/

			if (!bModbusError)
			{
				for (uint8_t i = 0; i < NUM_MK_READ; i++)
				{
					bPSUTimeout[i] = false;
					bPSUOverrun[i] = false;

					zmkZap = MK_ZAP[i];

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					zmkZap = MK_ZAP[i] | 0x0040;

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 1
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					zmkZap = MK_ZAP[i];

					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
					}

					if (rc == -1)
					{
						bModbusError = true;
						break;
					}

					TimerReadCurrent->reset();
					wxThread::Sleep(DELAY_CURRENT_READY);

					while (1)
					{
						{
							wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
							rc = modbus_read_registers(mbMaster->ctxRTU, mbMaster->ODC_Address, 1, &rdBuffer);
						}

						if (rc == -1)
						{
							bModbusError = true;
							break;
						}

						//Check reading
						if (!(rdBuffer & 0x4000)) //B14 = 0 (ready)
						{
							if ((rdBuffer & 0x1000) && CHECK_CURRENT_ERRORS) bPSUOverrun[i] = true; //B12 = 1
							else fCurrent[i] = (rdBuffer & 0x0FFF) / ADC_1A_READ;
							break;
						}

						if (CHECK_CURRENT_ERRORS)
						{
							//B14 = 1, continue reading
							wxThread::Sleep(DELAY_CURRENT_REPEAT);

							if (TimerReadCurrent->elapsed() > READ_CURRENT_TIMEOUT)
							{
								bPSUTimeout[i] = true;
								break;
							}
						}
						else
						{
							fCurrent[i] = 0;
							break;
						}
					}

					UpdateModbusData();

					wxThread::Sleep(DELAY_CURRENT_NEXTUNIT);

					if (bModbusError) break;
				}
			}

			/************************************ Other ************************************************/

			//Signal program to start main window
			if ((!CHECK_INIT_ERRORS || bInitSuccess) && !mbMaster->bStartMain) mbMaster->bStartMain = true; //main program
			else if (!bInitSuccess && !mbMaster->bStartMainError) mbMaster->bStartMainError = true; //error mode

			UpdateModbusData();
		}
	}

	return (wxThread::ExitCode) 0;
}

void cThreadReadCurrentRTU::UpdateModbusData(void)
{
	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		//Update polarity information
		ZNA = mbMaster->ZNA;

		//Store data in Modbus master
		mbMaster->bModbusError = bModbusError;
		mbMaster->bZMKPowered = !(IOExpander & 1);
		mbMaster->eActiveTract = eActiveTract;

		for (uint8_t i = 0; i < NUM_MK_READ; i++)
		{
			mbMaster->bPSUTimeout[i] = bPSUTimeout[i];
			mbMaster->bPSUOverrun[i] = bPSUOverrun[i];
			if (STORE_CURRENT_READINGS) mbMaster->fCurrent[i] = fCurrent[i];
		}
	}

	/************************************ Remote Data ******************************************/

	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

		//Store Modbus RTU Errors and ZMK Connection Status
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_ZMK_STATUS - 1] = (uint16_t) (!(IOExpander & 1));

		//Store Active Tract
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_ACTIVE_TRACT - 1] = (uint16_t) ((IOExpander >> 1) & 1);

		//Zero PSU-Error Registers
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_TIMEOUT - 1] = 0;
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_OVERRUN - 1] = 0;

		for (uint8_t i = 0; i < NUM_MK_READ; i++)
		{
			//Convert reading to string
			strCurrent = wxString::Format(wxT("%.2f"), fCurrent[i]);

			//Store polarity as MSB of BCD current
			if ((ZNA & MK_ZNA[i]) != 0) mbMaster->mb_mapping->tab_input_registers[i] = 0x8000;
			else mbMaster->mb_mapping->tab_input_registers[i] = 0;

			//Store reading in BCD format
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[0] - '0') << 8;
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[2] - '0') << 4;
			mbMaster->mb_mapping->tab_input_registers[i] |= (strCurrent[3] - '0');

			//PSU Timeout Errors
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_TIMEOUT - 1] |= ((uint16_t) bPSUTimeout[i]) << i;

			//PSU Overrun Errors
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_PSU_OVERRUN - 1] |= ((uint16_t) bPSUOverrun[i]) << i;
		}
	}
}

bool cThreadReadCurrentRTU::InitialisePSU(void)
{
	bool bSetCurrent = true;
	bool bCurrentOK = false;
	TimerInitPSU->reset();

	while (1)
	{
		for (uint8_t i = 0; i < NUM_MK_READ; i++)
		{
			bPSUTimeout[i] = false;
			bPSUOverrun[i] = false;

			zmkZap = MK_ZAP[i];

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
			}

			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			zmkZap = MK_ZAP[i] | 0x0040;

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 1
			}

			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			zmkZap = MK_ZAP[i];

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZAP_Address, zmkZap); //B6 = 0
			}

			if (rc == -1)
			{
				bModbusError = true;
				break;
			}

			TimerReadCurrent->reset();
			wxThread::Sleep(DELAY_CURRENT_READY);

			while (1)
			{
				{
					wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
					rc = modbus_read_registers(mbMaster->ctxRTU, mbMaster->ODC_Address, 1, &rdBuffer);
				}

				if (rc == -1)
				{
					bModbusError = true;
					break;
				}

				//Check reading
				if (!(rdBuffer & 0x4000)) //B14 = 0 (ready)
				{
					if ((rdBuffer & 0x1000) && CHECK_CURRENT_ERRORS) bPSUOverrun[i] = true; //B12 = 1
					else fCurrent[i] = (rdBuffer & 0x0FFF) / ADC_1A_READ;
					break;
				}

				if (CHECK_CURRENT_ERRORS)
				{
					//B14 = 1, continue reading
					wxThread::Sleep(DELAY_CURRENT_REPEAT);

					if (TimerReadCurrent->elapsed() > READ_CURRENT_TIMEOUT)
					{
						bPSUTimeout[i] = true;
						break;
					}
				}
				else
				{
					fCurrent[i] = 0;
					break;
				}
			}

			UpdateModbusData(); //update Local/Remote data
			if (bModbusError) break; //check for errors
			wxThread::Sleep(DELAY_CURRENT_NEXTUNIT); //delay reading next unit
		}

		/******************************* Update Local/Remote Data ***********************************/

		UpdateModbusData();

		/************************************ Zero Current ******************************************/

		if (!bModbusError)
		{
			//Set current to zero for all PSUs
			if (bSetCurrent)
			{
				for (uint8_t unitID = 1; unitID <= NUM_POWER_UNITS; unitID++) mbMaster->SetCurrent(unitID, 0);
				bSetCurrent = false;
			}

			//Reset flag
			bCurrentOK = true;

			//Check current readings
			for (uint8_t i = 0; i < NUM_MK_READ; i++)
			{
				if (bPSUTimeout[i] || bPSUOverrun[i])
				{
					bCurrentOK = false; //read-current errors, unable to check
					break;
				}
				else if (fCurrent[i] >= MIN_SAFE_CURRENT) //current > 0.1A
				{
					bCurrentOK = false;
					break;
				}
			}
		}

		if (bCurrentOK)
		{
			break; //initialisation successful
		}
		else if (TimerInitPSU->elapsed() >= INIT_PSU_TIMEOUT)
		{
			for (uint8_t i = 0; i < NUM_MK_READ; i++)
			{
				if (bPSUTimeout[i] || bPSUOverrun[i] || fCurrent[i] >= MIN_SAFE_CURRENT)
				{
					wxCriticalSectionLocker local_lock(mbMaster->local_guard);
					mbMaster->bInitTimeout[i] = true;
				}
			}

			break; //initialisation timeout
		}
	}

	/************************************ Initialise ZNA Registers ******************************/

	if (bCurrentOK)
	{
		//Initialise ZNA register
		wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
		rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address, 0);

		//Initialization successful
		return true;
	}
	else return false; //Initialization failed
}

cThreadReadCurrentRTU::~cThreadReadCurrentRTU()
{
	wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
	modbus_close(mbMaster->ctxRTU);
	modbus_free(mbMaster->ctxRTU);
	mbMaster->ThreadReadCurrentRTU = NULL;
}

/**************************************** Set-Current Thread ***********************************************/

cThreadSetCurrentRTU::cThreadSetCurrentRTU(cModbusMaster *mbMaster, uint8_t unitID, float fNewCurrentSetting) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->fNewCurrentSetting = fabs(fNewCurrentSetting);
    this->unitID = unitID;
    ui16CurrentSetting = 0;
    bNegativePolarity = false;
    rc = -1;

    {
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		bModbusError = mbMaster->bModbusError;
		fCurrentSetting = fabs(mbMaster->fCurrentSetting[unitID - 1]);
		ZNA = mbMaster->ZNA;
    }
}

wxThread::ExitCode cThreadSetCurrentRTU::Entry()
{
	if (!bModbusError)
	{
		if (unitID < TK_START) bNegativePolarity = ((ZNA & MK_ZNA[unitID - 1]) != 0);

		if (fabs(fCurrentSetting - fNewCurrentSetting) > H_AMP) //step current change (> 0.50A)
		{
			if (fNewCurrentSetting > fCurrentSetting) //upward step
			{
				while (fCurrentSetting != fNewCurrentSetting && !TestDestroy())
				{
					//Calculate new current step
					if ((fNewCurrentSetting - fCurrentSetting) > H_AMP) fCurrentSetting += H_AMP;
					else fCurrentSetting = fNewCurrentSetting;

					//Calculate 12-bit setting
					if (unitID < TK_START) ui16CurrentSetting = ((uint16_t) round(fCurrentSetting * ADC_1A_WRITE_MK)) & 0xFFF;
					else ui16CurrentSetting = ((uint16_t) round(fCurrentSetting * ADC_1A_WRITE_TK)) & 0xFFF;

					//Append DAC command
					ui16CurrentSetting |= (mbMaster->MK_DAC[unitID - 1][1] | 0x1000);

					//Send command
					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->MK_DAC[unitID - 1][0], ui16CurrentSetting);
					}

					//Delay next current step
					wxThread::Sleep(DELAY_CURRENT_STEP);
				}
			}
			else if (fNewCurrentSetting < fCurrentSetting)//downward step
			{
				while (fCurrentSetting != fNewCurrentSetting && !TestDestroy())
				{
					//Calculate new current step
					if ((fCurrentSetting - fNewCurrentSetting) > H_AMP) fCurrentSetting -= H_AMP;
					else fCurrentSetting = fNewCurrentSetting;

					//Calculate 12-bit setting
					if (unitID < TK_START) ui16CurrentSetting = ((uint16_t) round(fCurrentSetting * ADC_1A_WRITE_MK)) & 0xFFF;
					else ui16CurrentSetting = ((uint16_t) round(fCurrentSetting * ADC_1A_WRITE_TK)) & 0xFFF;

					//Append DAC command
					ui16CurrentSetting |= (mbMaster->MK_DAC[unitID - 1][1] | 0x1000);

					//Send command
					{
						wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
						rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->MK_DAC[unitID - 1][0], ui16CurrentSetting);
					}

					//Delay next current step
					wxThread::Sleep(DELAY_CURRENT_STEP);
				}
			}
		}
		else if (!TestDestroy()) //immediate current change
		{
			//Calculate 12-bit setting
			if (unitID < TK_START) ui16CurrentSetting = ((uint16_t) round(fNewCurrentSetting * ADC_1A_WRITE_MK)) & 0xFFF;
			else ui16CurrentSetting = ((uint16_t) round(fNewCurrentSetting * ADC_1A_WRITE_TK)) & 0xFFF;

			//Append DAC command
			ui16CurrentSetting |= (mbMaster->MK_DAC[unitID - 1][1] | 0x1000);

			//Send command
			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
				rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->MK_DAC[unitID - 1][0], ui16CurrentSetting);
			}
		}
	}

	//Update Modbus data
	UpdateModbusDataWrite();

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

void cThreadSetCurrentRTU::UpdateModbusDataWrite(void)
{
	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		if (bNegativePolarity && fNewCurrentSetting != 0) mbMaster->fCurrentSetting[unitID - 1] = fNewCurrentSetting * (-1);
		else mbMaster->fCurrentSetting[unitID - 1] = fNewCurrentSetting;
		if (!STORE_CURRENT_READINGS) mbMaster->fCurrent[unitID - 1] = fNewCurrentSetting;
	}

	/************************************ Remote Data ******************************************/

	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

		//Convert setting to string
		strCurrentSetting = wxString::Format(wxT("%.2f"), fNewCurrentSetting);

		//Store polarity as MSB of BCD
		if (bNegativePolarity && fNewCurrentSetting != 0) mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] = 0x8000;
		else mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] = 0;

		//Convert to BCD
		if (fNewCurrentSetting < 10) //values < 10A
		{
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[0] - '0') << 8;
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[2] - '0') << 4;
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[3] - '0');
		}
		else if (fNewCurrentSetting >= 10) //values >= 10A (TK Magnets)
		{
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[0] - '0') << 12;
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[1] - '0') << 8;
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[3] - '0') << 4;
			mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_SETCUR_START + (unitID - 2)] |= (strCurrentSetting[4] - '0');
		}
	}
}

cThreadSetCurrentRTU::~cThreadSetCurrentRTU()
{
	mbMaster->ThreadSetCurrentRTU[unitID - 1] = NULL;
}

/**************************************** Set Polarity Thread **********************************************/

cThreadSetPolarityRTU::cThreadSetPolarityRTU(cModbusMaster *mbMaster, uint8_t unitID, bool bSetNegative) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->unitID = unitID;
    this->bSetNegative = bSetNegative;
	rc = -1;

	wxCriticalSectionLocker local_lock(mbMaster->local_guard);
	bModbusError = mbMaster->bModbusError;
	ZNA = mbMaster->ZNA;
}

wxThread::ExitCode cThreadSetPolarityRTU::Entry()
{
	if (!bModbusError)
	{
		//Update polarity settings
		if (bSetNegative) ZNA |= MK_ZNA[unitID - 1]; //set bit
		else ZNA &= ~MK_ZNA[unitID - 1]; //clear bit

		{
			//Write new polarity settings
			wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
			rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address, ZNA);
		}

		/************************************ Local Data *******************************************/

		{
			wxCriticalSectionLocker local_lock(mbMaster->local_guard); //prevent Local data access

			if (rc != -1)
			{
				if (bSetNegative) mbMaster->ZNA |= MK_ZNA[unitID - 1]; //set bit
				else mbMaster->ZNA &= ~MK_ZNA[unitID - 1]; //clear bit
			}
		}

		/************************************ Remote Data ******************************************/

		{
			wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

			if (rc != -1)
			{
				if (bSetNegative) mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_POLARITY - 1] |= MK_ZNA[unitID - 1]; //set bit
				else mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_POLARITY - 1] &= ~MK_ZNA[unitID - 1]; //clear bit
			}
		}
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadSetPolarityRTU::~cThreadSetPolarityRTU()
{
	mbMaster->ThreadSetPolarityRTU[unitID - 1] = NULL;
}
