#pragma once

#include "ZMKConstants.h"
#include "cThreadSetCurrent.h"
#include "cInformation.h"
#include "cPowerCheck.h"
#include "cModbusMaster.h"

class cEntry;
class cModbusMaster;
class cInformation;
class cPowerCheck;
class cTimerHighRes;
class cThreadSetTract;
class cThreadSetCurrent;

class cMain : public wxFrame
{
	public: //constructor
		cMain(cEntry*);
		void TerminateApp(bool);

	private: //event handlers
		void OnMenuClickedInformation(wxCommandEvent& evt);
		void OnMenuClickedMode(wxCommandEvent& evt);
		void OnMenuClickedExit(wxCommandEvent& evt);
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnButtonSwitchTract(wxCommandEvent& evt);
		void OnButtonSetCurrent(wxCommandEvent& evt);
		void OnRemoteSetCurrent(wxCommandEvent& evt);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cModbusMaster* mbMaster;
		cInformation* pInformation;
		cPowerCheck* pPowerCheck;
		wxMenu* mHelpMenu;
		bool informationWindowIsOpen;

	public: //other public variables
		wxCriticalSection main_guard; //main data guard
		wxCriticalSection event_guard; //TCP/IP event data guard
		eMode eUserMode;
		bool bSetCurrentError[NUM_POWER_UNITS] = { false };

	public: //remote control
		uint8_t fRemoteUnitFIFO[MAX_REMOTE_EVENTS] = { 0 };
		float fRemoteCurrentFIFO[MAX_REMOTE_EVENTS] = { 0 };
		uint8_t newCurrentNum;

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mCloseMenu;
		wxMenu* mModeMenu;

	private: //UI elements

		//PANELS
		wxPanel* panTractDisplay;
		wxPanel* panControls;
		wxPanel* panZMKPower;
		wxPanel* panMK[6] = { NULL };
		wxPoint panPosition;

		//IMAGES
		wxStaticBitmap* imgTracts[NUM_TRACTS];
		wxStaticBitmap* imgTractsMBError[NUM_TRACTS];
		wxStaticBitmap* imgZMKPowered;
		wxStaticBitmap* imgZMKNoPower;
		wxStaticBitmap* imgModbusError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentError[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentSet[NUM_POWER_UNITS];
		wxStaticBitmap* imgCurrentOK[NUM_POWER_UNITS];

		//FONTS
		wxFont myFont;

		//STATIC TEXTS
		wxStaticText* txtTitleLI;
		wxStaticText* txtTitleControls;
		wxStaticText* txtTitleZMKConnected;
		wxStaticText* txtZMKPoweredMBError;
		wxStaticText* txtCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//CONTROLS
		wxTextCtrl* txtCtrlCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//BUTTONS
		wxButton* btnCurrentZMK[NUM_POWER_UNITS] = { NULL };

		//THREADS
		cThreadSetCurrent* ThreadSetCurrent[NUM_POWER_UNITS] = { NULL };

	private: //Shared Data

		//Errors
		bool bModbusError;
		bool bPSUTimeout[NUM_POWER_UNITS] = { false }; //last 3 entries always false
		bool bPSUOverrun[NUM_POWER_UNITS] = { false }; //last 3 entries always false

		//Readings
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		float fCurrentSetting[NUM_POWER_UNITS] = { 0 };

		//Other
		uint16_t ZNA;
		eTract eActiveTract;
		eTract eNewTract;
		bool bZMKPowered;

	private: //other data

		wxTimer* tRefreshTimer;
		cTimerHighRes* TimerCloseApp;
		uint8_t mkNum;
		uint8_t zmkNum;
		uint8_t zmkIndex;
		bool bCurrentOK;

		//Friend classes
		friend class cThreadSetTract;
		friend class cThreadSetCurrent;

		wxDECLARE_EVENT_TABLE();
};


