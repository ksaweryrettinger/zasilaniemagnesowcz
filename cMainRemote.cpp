#include "cMain.h"

void cMain::OnRemoteSetCurrent(wxCommandEvent& evt)
{
	uint8_t unitID;
	float fNewCurrent;

	{
		wxCriticalSectionLocker event_lock(event_guard);

		//Get data from FIFO buffer
		fNewCurrent = fRemoteCurrentFIFO[0];
		unitID = fRemoteUnitFIFO[0];

		//Shift FIFO buffer
		for (uint8_t i = 1; i <= newCurrentNum; i++)
		{
			fRemoteUnitFIFO[i - 1] = fRemoteUnitFIFO[i];
			fRemoteCurrentFIFO[i - 1] = fRemoteCurrentFIFO[i];
		}

		fRemoteUnitFIFO[newCurrentNum] = 0;
		fRemoteCurrentFIFO[newCurrentNum] = 0;
		newCurrentNum--;
	}

	if (!mbMaster->ThreadSetCurrentRTU[unitID - 1])
	{
		//Check current limits
		if (unitID < TK_START) //MK and LI Magnets
		{
			if (fNewCurrent < (MAX_CURRENT_MK * (-1))) fNewCurrent = MAX_CURRENT_MK * (-1);
			else if (fNewCurrent > MAX_CURRENT_MK) fNewCurrent = MAX_CURRENT_MK;
			txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));
		}
		else //TK Magnet
		{
			if (fNewCurrent < 0) fNewCurrent = 0;
			else if (fNewCurrent > MAX_CURRENT_TK) fNewCurrent = MAX_CURRENT_TK;
			txtCtrlCurrentZMK[unitID - 1]->SetValue(wxString::Format(wxT("%.2f"), fNewCurrent));
		}

		//Start new set-current thread
		ThreadSetCurrent[unitID - 1] = new cThreadSetCurrent(this, mbMaster, unitID, fNewCurrent);

		if (ThreadSetCurrent[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create set-current thread!");
			delete ThreadSetCurrent[unitID - 1];
			ThreadSetCurrent[unitID - 1] = NULL;
		}
	}

	evt.Skip();
}
