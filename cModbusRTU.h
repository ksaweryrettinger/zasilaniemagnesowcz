#pragma once

#include "ZMKConstants.h"
#include "cModbusMaster.h"
#include "cTimerHighRes.h"

class cMain;
class cModbusMaster;
class cThreadReadCurrentRTU;
class cThreadSetCurrentRTU;
class cThreadSetPolarityRTU;
class cTimerHighRes;

/* Looping thread, used to read Modbus holding registers */
class cThreadReadCurrentRTU : public wxThread
{
	public:
		cThreadReadCurrentRTU(cModbusMaster*);
		~cThreadReadCurrentRTU();
		bool InitialisePSU(void);

	protected:
		virtual ExitCode Entry();

	private:
		void UpdateModbusData(void);

	private:
		cModbusMaster* mbMaster;
		int32_t rc;
		int32_t ct;

	private: //data shared with Modbus Master
		eTract eActiveTract;
		uint16_t ZNA;
		uint16_t IOExpander;
		float fCurrent[NUM_POWER_UNITS] = { 0 };
		bool bPSUTimeout[NUM_POWER_UNITS] = { false };
		bool bPSUOverrun[NUM_POWER_UNITS] = { false };
		bool bModbusError;

	private: //other
		cTimerHighRes* TimerInitPSU;
		cTimerHighRes* TimerReadCurrent;
		uint16_t zmkZap;
		uint16_t rdBuffer;
		wxString strCurrent;
		bool rdStatus;
		bool bInitSuccess;
};

/* Temporary thread, used to write new current value to PSU */
class cThreadSetCurrentRTU : public wxThread
{
	public:
		cThreadSetCurrentRTU(cModbusMaster*, uint8_t, float);
		~cThreadSetCurrentRTU();

	protected:
		virtual ExitCode Entry();

	private:
		void UpdateModbusDataWrite(void);

	private:
		cModbusMaster* mbMaster;
		uint8_t unitID;
		int32_t rc;
		wxString strCurrentSetting;
		bool bNegativePolarity;

	private: //data shared with Modbus master
		bool bModbusError;
		float fCurrentSetting;
		float fNewCurrentSetting;
		uint16_t ui16CurrentSetting;
		uint16_t ZNA;
};

/* Temporary thread, used to set new PSU polarity */
class cThreadSetPolarityRTU : public wxThread
{
	public:
		cThreadSetPolarityRTU(cModbusMaster*, uint8_t, bool);
		~cThreadSetPolarityRTU();

	protected:
		virtual ExitCode Entry();
		cModbusMaster* mbMaster;

	private:
		bool bSetNegative;
		bool bModbusError;
		int32_t rc;
		uint16_t ZNA;
		uint8_t unitID;
};

